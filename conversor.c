/*programa de aula do git*/

#include <stdio.h>

int main() {
	
	int n;
	int hidrog, helio, grav, gas;

	scanf("%d", &n);

	gas = n / 50;
	n = n - 50 * gas;
	grav = n / 10;
	n = n - 10 * grav;
	helio = n / 5;
	n = n - 5 * helio;
	
	
	hidrog = n;

	printf("%d hidrogenio\n%d helio\n%d gravidade\n%d gas\n", hidrog, helio, grav, gas);

	return 0;
}